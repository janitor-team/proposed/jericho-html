#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

# clean up the upstream tarball
unzip $3
rm -rf jericho-html-$2/bin jericho-html-$2/doc jericho-html-$2/lib \
  jericho-html-$2/samples/CommandLine/bin
XZ_OPT=--best tar -c -J -f ../jericho-html_$2.orig.tar.xz \
  --exclude *.jar \
  --exclude *.class \
  --exclude docs/javadoc/* \
  jericho-html-$2
rm -rf jericho-html-$2 $3
